#!/bin/sh
emacs \
    -q \
    --eval "(setq ert-batch-backtrace-right-margin $1)" \
    -batch \
    -l ttcn-3-mode.el \
    -l tests.el \
    -f ert-run-tests-batch-and-exit \
