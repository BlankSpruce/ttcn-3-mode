(require 'seq)
(require 'semantic/symref/grep)

(defgroup ttcn-3-mode nil
  "TTCN-3 major mode group"
  :link '(url-link "http://www.blukaktus.com/TTCN3QRC_viewme.pdf")
  :group 'languages
  )

(defcustom ttcn-3-indent-offset 4
  "Number of spaces to indent by"
  :type 'integer
  :group 'ttcn-3-mode
  :safe #'integerp
  )

(defcustom ttcn-3-use-alternative-comment-style nil
  "Use alternative commenting style where selected
region is commented inline inside /* */"
  :type 'boolean
  :group 'ttcn-3-mode
  :safe #'booleanp
  )

(defconst ttcn-3-boolean-constants
  '("false" "true")
  )

(defconst ttcn-3-verdict-type-constants
  '("error" "fail" "inconc" "none" "pass")
  )

(defconst ttcn-3-float-constants
  '("not_a_number")
  )

(defconst ttcn-3-address-type-contants
  '("null")
  )

(defconst ttcn-3-optional-type-constants
  '("omit")
  )

(defconst ttcn-3-constants
  (append
   ttcn-3-boolean-constants
   ttcn-3-verdict-type-constants
   ttcn-3-float-constants
   ttcn-3-address-type-contants
   ttcn-3-optional-type-constants
   )
  )

(defconst ttcn-3-module-keywords
  '("all" "except" "from" "group" "import" "language" "module")
  )

(defconst ttcn-3-visibility-keywords
  '("friend" "private" "public")
  )

(defconst ttcn-3-reference-operation-keywords
  '("mtc" "self" "system")
  )

(defconst ttcn-3-component-keywords
  '("extends")
  )

(defconst ttcn-3-port-keywords
  '("any" "address" "in" "inout" "map" "message" "out" "port" "procedure" "unmap")
  )

(defconst ttcn-3-procedure-signature-keywords
  '("exception" "in" "inout" "out" "noblock" "signature")
  )

(defconst ttcn-3-types-and-templates-declaration-keywords
  '("const" "modifies" "modulepar" "template" "type" "valueof" "var")
  )

(defconst ttcn-3-template-restriction-keywords
  '("omit" "present" "value")
  )

(defconst ttcn-3-template-matching-keywords
  '("complement" "decmatch" "ifpresent" "infinity" "length" "omit" "pattern"
    "permutation" "subset" "superset")
  )

(defconst ttcn-3-behaviour-block-keywords
  '("altstep" "control" "function" "interleave" "mtc" "return" "runs on"
    "system" "testcase")
  )

(defconst ttcn-3-statement-and-operations-keywords
  '("action" "activate" "alt" "break" "case" "continue" "deactivate" "do" "else"
    "execute" "for" "goto" "if" "interleave" "label" "log" "match" "read"
    "repeat" "running" "select" "start" "stop" "timeout" "while")
  )

(defconst ttcn-3-operator-keywords
  '("and" "and4b" "mod" "not" "not4b" "or" "or4b" "rem" "xor" "xor4b")
  )

(defconst ttcn-3-message-based-communication-operation-keywords
  '("check" "send" "receive" "to" "trigger")
  )

(defconst ttcn-3-procedure-based-communication-operation-keywords
  '("call" "catch" "check" "getcall" "getreply" "nowait" "param" "raise" "reply"
    "sender")
  )

(defconst ttcn-3-port-operation-keywords
  '("checkstate" "clear" "external" "halt" "start" "stop")
  )

(defconst ttcn-3-timer-operation-keywords
  '("start" "stop" "read" "running" "timeout")
  )

(defconst ttcn-3-timer-and-alternatives-keywords
  '("alt" "interleave" "timer")
  )

(defconst ttcn-3-dynamic-configuration-keywords
  '("alive" "connect" "create" "disconnect" "done" "kill" "killed" "map"
    "running" "setencode" "start" "stop" "unmap")
  )

(defconst ttcn-3-test-verdict-keywords
  '("setverdict" "getverdict")
  )

(defconst ttcn-3-with-statement-keywords
  '("display" "encode" "extension" "optional" "override" "variant" "with")
  )

(defconst ttcn-3-deprecated-keywords
  '("mixed" "recursive")
  )

(defconst ttcn-3-keywords
  (append
   ttcn-3-module-keywords
   ttcn-3-visibility-keywords
   ttcn-3-reference-operation-keywords
   ttcn-3-component-keywords
   ttcn-3-port-keywords
   ttcn-3-procedure-signature-keywords
   ttcn-3-types-and-templates-declaration-keywords
   ttcn-3-template-restriction-keywords
   ttcn-3-template-matching-keywords
   ttcn-3-behaviour-block-keywords
   ttcn-3-statement-and-operations-keywords
   ttcn-3-operator-keywords
   ttcn-3-message-based-communication-operation-keywords
   ttcn-3-procedure-based-communication-operation-keywords
   ttcn-3-port-operation-keywords
   ttcn-3-timer-operation-keywords
   ttcn-3-timer-and-alternatives-keywords
   ttcn-3-dynamic-configuration-keywords
   ttcn-3-test-verdict-keywords
   ttcn-3-with-statement-keywords
   ttcn-3-deprecated-keywords
   )
  )

(defconst ttcn-3-conversion-functions
  '("any2unistr" "bit2hex" "bit2int" "bit2oct" "bit2str" "char2int" "char2oct"
    "enum2int" "float2int" "hex2bit" "hex2int" "hex2oct" "hex2str" "int2bit"
    "int2char" "int2enum" "int2float" "int2hex" "int2oct" "int2str"
    "int2unichar" "oct2bit" "oct2char" "oct2hex" "oct2int" "oct2str"
    "oct2unichar" "str2float" "str2hex" "str2int" "str2oct" "unichar2int"
    "unichar2oct")
  )

(defconst ttcn-3-length-functions
  '("lengthof" "sizeof")
  )

(defconst ttcn-3-presence-checking-functions
  '("isbound" "ischosen" "ispresent" "istemplatekind" "isvalue")
  )

(defconst ttcn-3-string-handling-functions
  '("regexp" "replace" "substr")
  )

(defconst ttcn-3-codec-functions
  '("decvalue" "decvalue_o" "decvalue_unichar" "encvalue" "encvalue_o"
    "encvalue_unichar" "get_stringencoding" "remove_bom")
  )

(defconst ttcn-3-other-functions
  '("char" "hostid" "rnd" "testcasename")
  )

(defconst ttcn-3-predefined-functions
  (append
   ttcn-3-conversion-functions
   ttcn-3-length-functions
   ttcn-3-presence-checking-functions
   ttcn-3-string-handling-functions
   ttcn-3-codec-functions
   ttcn-3-other-functions
   )
  )

(defconst ttcn-3-simple-basic-types
  '("boolean" "float" "integer")
  )

(defconst ttcn-3-basic-string-types
  '("bitstring" "charstring" "hexstring" "octetstring" "universal charstring")
  )

(defconst ttcn-3-special-types
  '("anytype" "address" "component" "default" "objid" "port" "verdicttype")
  )

(defconst ttcn-3-structured-types
  '("anytype" "enumerated" "record of" "record" "set of" "set" "union")
  )

(defconst ttcn-3-types
  (append
   ttcn-3-simple-basic-types
   ttcn-3-basic-string-types
   ttcn-3-special-types
   ttcn-3-structured-types
   )
  )

(defconst ttcn-3-font-lock-keywords
  (append
    `(
      (, (regexp-opt ttcn-3-constants 'words)            . font-lock-constant-face)
      (, (regexp-opt ttcn-3-keywords 'words)             . font-lock-keyword-face)
      (, (regexp-opt ttcn-3-predefined-functions 'words) . font-lock-builtin-face)
      (, (regexp-opt ttcn-3-types 'words)                . font-lock-type-face)
      )
    )
  )

(defvar ttcn-3-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; Comments
    (modify-syntax-entry ?/  ". 124" table)
    (modify-syntax-entry ?*  ". 23b" table)
    (modify-syntax-entry ?\n ">"     table)

    ;; Operators
    (dolist (op '(?+ ?- ?& ?< ?> ?@ ?! ?: ?=))
      (modify-syntax-entry op "." table)
      )

    ;; Strings
    (modify-syntax-entry ?\" "\"" table)
    (modify-syntax-entry ?\\ "\\" table)

    ;; Word constituents
    (modify-syntax-entry ?_ "w" table)

    ;; Parentheses
    (modify-syntax-entry ?\( "()" table)
    (modify-syntax-entry ?\) ")(" table)

    (modify-syntax-entry ?\[ "(]" table)
    (modify-syntax-entry ?\] ")[" table)

    (modify-syntax-entry ?\{ "(}" table)
    (modify-syntax-entry ?\} "){" table)

    table)
  "Syntax table for TTCN-3 major mode"
  )

(defun ttcn-3--parentheses-level ()
  (nth 0 (syntax-ppss))
  )

(defun ttcn-3--parent-scope-start ()
  (or (nth 1 (syntax-ppss)) 0)
  )

(defun ttcn-3--parent-scope-end ()
  (save-excursion
    (ignore-errors (up-list))
    (or (point) (point-max))
    )
  )

(defun ttcn-3--parentheses-level-at-point (p)
  (save-excursion
    (goto-char p)
    (ttcn-3--parentheses-level)
    )
  )

(defun ttcn-3--indentation-level ()
  (/ (current-indentation) ttcn-3-indent-offset)
  )

(defun ttcn-3--indentation-level-at-point (p)
  (save-excursion
    (goto-char p)
    (ttcn-3--indentation-level)
    )
  )

(defun ttcn-3--parent-scope-ends-in-current-line-p ()
  (let ((beginning-line (line-number-at-pos (line-beginning-position)))
        (scope-end-line (line-number-at-pos (ttcn-3--parent-scope-end)))
        )
    (= beginning-line scope-end-line)
    )
  )

(defun ttcn-3--potential-indentation-levels ()
  (list
   (ttcn-3--parentheses-level-at-point (line-beginning-position))
   (ttcn-3--parentheses-level-at-point (line-end-position))
   (if (ttcn-3--parent-scope-ends-in-current-line-p)
       (ttcn-3--indentation-level-at-point (ttcn-3--parent-scope-start))
     (+ 1 (ttcn-3--indentation-level-at-point (ttcn-3--parent-scope-start)))
     )
   )
  )

(defun ttcn-3--sufficient-indentation-level ()
  (seq-min (ttcn-3--potential-indentation-levels))
  )

(defun ttcn-3--within-indentation-p ()
  (let* ((p (point))
         (beginning (line-beginning-position))
         (indented-beginning (+ beginning (current-indentation)))
         )
    (and (<= beginning p)
         (>= indented-beginning p)
         )
    )
  )

(defun ttcn-3-indent-line ()
  (interactive)
  (save-excursion
    (let ((level (ttcn-3--sufficient-indentation-level)))
      (indent-line-to (* level ttcn-3-indent-offset))
      )
    )
  (when (ttcn-3--within-indentation-p)
    (back-to-indentation)
    )
  )

(defvar ttcn-3-mode-map
  (let ((map (make-sparse-keymap)))
    map)
  "Keymap for TTCN-3 major mode"
  )



(defun ttcn-3-mode-set-comment-style ()
  (if ttcn-3-use-alternative-comment-style
      (progn
        (setq-local comment-start "/*")
        (setq-local comment-end "*/")
        )
    (setq-local comment-start "//")
    )
  )

(define-derived-mode ttcn-3-mode prog-mode "TTCN-3"
  "Major mode for TTCN-3

\\{ttcn-3-mode-map}"
  :group 'ttcn-3-mode
  :syntax-table ttcn-3-mode-syntax-table

  ;; Comment style
  (add-hook 'ttcn-3-mode-hook 'ttcn-3-mode-set-comment-style)

  ;; Keywords
  (setq-local font-lock-defaults '((ttcn-3-font-lock-keywords)))

  ;; Indentation
  (setq-local indent-line-function 'ttcn-3-indent-line)

  ;; Electricity on closing parentheses
  (dolist (char '(?\) ?\] ?\}))
    (add-to-list 'electric-indent-chars char)
    )
  )

(add-to-list 'auto-mode-alist '("\\.ttcn3" . ttcn-3-mode))
(add-to-list 'semantic-symref-filepattern-alist '(ttcn-3-mode "*.ttcn3"))
(add-hook 'ttcn-3-mode-hook (lambda () (semantic-mode)))

(provide 'ttcn-3-mode)
