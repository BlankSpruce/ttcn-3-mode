(require 'ttcn-3-mode)
(require 'ert)

(defun run-indent-test (original expected)
  (with-temp-buffer
    (ttcn-3-mode)
    (setq indent-tabs-mode nil)
    (insert original)
    (indent-region (point-min) (point-max))
    (should (equal (buffer-string) expected))
    )
  )

(defmacro def-indent-test (name original expected)
  `(ert-deftest ,(intern (format "indent-test--%s" name)) ()
     (run-indent-test ,original ,expected)
     )
  )

(def-indent-test basic
"
module foo
{
type record Bar
{
integer baz
};
}
"
"
module foo
{
    type record Bar
    {
        integer baz
    };
}
"
)

(def-indent-test nested-parentheses
"
template Foo foo :=
{
bar :=
{
baz :=
{
qux :=
{
val := 3
}
}
}
}
"
"
template Foo foo :=
{
    bar :=
    {
        baz :=
        {
            qux :=
            {
                val := 3
            }
        }
    }
}
"
)

(def-indent-test nested-scope-with-inlined-opening-parentheses
"
const integer foo := calculate_foo({
bar := 123,
baz := 456,
qux := {{{
quux := 789
}}}
})
"
"
const integer foo := calculate_foo({
    bar := 123,
    baz := 456,
    qux := {{{
        quux := 789
    }}}
})
"
)

(def-indent-test previous-scope-closes-and-new-one-starts-in-the-same-line
"
template Foo foo(
template integer a,
template integer b
) := other_foo_template(
c := a+b
d := a*b
)
"
"
template Foo foo(
    template integer a,
    template integer b
) := other_foo_template(
    c := a+b
    d := a*b
)
"
)
